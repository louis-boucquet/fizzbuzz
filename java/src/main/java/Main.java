public class Main implements Runnable {
    public static void main(String[] args) {
        new Main().run();
    }

    public void run() {
        new FizzBuzzer<>(
                Config.triggers,
                (key, i) -> i % key == 0,
                (i, set) -> {
                    System.out.print(i);
                    System.out.print(' ');
                    System.out.println(set);
                })
                .buzzRange(Config.range);
    }
}

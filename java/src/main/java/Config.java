
import javax.json.*;
import java.io.*;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Config {
    public static final JsonObject config = readFile();
    public static final Map<Integer, String> triggers = makeTriggers();
    public static Iterable<Integer> range = makeIterable();

    private static JsonObject readFile() {
        try (
                FileReader fileReader = new FileReader(new File("config.json"));
                JsonReader reader = Json.createReader(fileReader);
        ) {
            return reader.readObject();
        } catch (FileNotFoundException e) {
            System.err.println("couldn't find config.json file!");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("couldn't read config.json file, is it in use by another process?");
            System.exit(1);
        }
        return null;
    }

    private static Map<Integer, String> makeTriggers() {
        assert config != null;
        JsonArray triggers = config.getJsonArray("triggers");
        if (triggers == null) {
            System.err.println("invalid config.json file");
            System.err.println("see documentation for more info");
            System.exit(1);
        }

        return triggers.getValuesAs(JsonObject.class)
                .stream()
                .collect(Collectors.toMap(
                        trigger -> trigger.getInt("number"),
                        trigger -> trigger.getString("word")
                ));
    }

    private static Iterable<Integer> makeIterable() {
        assert config != null;

        Map<String, Supplier<Iterable<Integer>>> options = new HashMap<>();
        options.put("list", Config::makeIterableFromList);
        options.put("range", Config::makeIterableFromRange);

        String option;
        if (!config.containsKey("type")) {
            System.out.println("no property type, falling back on default");
            option = "list";
        } else {
            option = config.getString("type");
        }


        Supplier<Iterable<Integer>> supplier = options.get(option);

        if (supplier == null) {
            System.err.println("invalid value for property type");
            System.err.println("see documentation for more info");
            System.exit(1);
        }

        return supplier.get();
    }

    private static Iterable<Integer> makeIterableFromRange() {
        assert config != null;

        String range = config.getString("range");

        if (range == null) {
            System.err.println("invalid config.json file");
            System.err.println("see documentation for more info");
            System.exit(1);
        }

        List<Integer> borders = Arrays.stream(range.split("-"))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        int start = borders.get(0);
        int size = borders.get(1) - borders.get(0);

        return () -> Stream
                .iterate(start, i -> ++i)
                .limit(size)
                .iterator();
    }

    private static Iterable<Integer> makeIterableFromList() {
        assert config != null;
        JsonArray triggers = config.getJsonArray("list");
        if (triggers == null) {
            System.err.println("invalid config.json file");
            System.err.println("see documentation for more info");
            System.exit(1);
        }

        return triggers
                .stream()
                .map(JsonValue::toString)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }
}

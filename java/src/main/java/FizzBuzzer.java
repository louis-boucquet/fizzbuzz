import java.util.Map;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collectors;

public class FizzBuzzer<K, V> {

    private Map<K, V> triggers;
    private BiFunction<K, K, Boolean> filter;
    private Consumer<Set<V>> handlerNormal;
    private BiConsumer<K, Set<V>> handlerSet;

    public FizzBuzzer(Map<K, V> triggers, BiFunction<K, K, Boolean> filter, Consumer<Set<V>> handler) {
        this(triggers, filter);
        this.handlerNormal = handler;
    }

    public FizzBuzzer(Map<K, V> triggers, BiFunction<K, K, Boolean> filter, BiConsumer<K, Set<V>> handler) {
        this(triggers, filter);
        this.handlerSet = handler;
    }

    private FizzBuzzer(Map<K, V> triggers, BiFunction<K, K, Boolean> filter) {
        this.triggers = triggers;
        this.filter = filter;
    }

    public void buzz(K i) {
        if (handlerNormal != null) {
            buzzNormal(i);
        } else {
            buzzSet(i);
        }
    }

    private void buzzNormal(K i) {
        handlerNormal.accept(triggers.keySet()
                .stream()
                .filter(key -> filter.apply(key, i))
                .map(key -> triggers.get(key))
                .collect(Collectors.toSet()));
    }

    public void buzzSet(K i) {
        handlerSet.accept(i, triggers.keySet()
                .stream()
                .filter(key -> filter.apply(key, i))
                .map(key -> triggers.get(key))
                .collect(Collectors.toSet()));
    }

    public void buzzRange(Iterable<K> iterable) {
        iterable.forEach(this::buzz);
    }
}
